# Water Levels Programming Test

Write a program in Rust or TypeScript that calculates the water level in different parts
of a landscape.
The landscape is defined as positive numbers. Examples: 3,1,6,4,8,9


```
10 +-----------------------------------------------------------------------+
   |                                                                       |
 9 |                                                         +-------------+
   |                                                         |             |
 8 |                                            +------------+             |
   |                                            |            |             |
 7 |                                            |            |             |
   |                                            |            |             |
 6 |                    +-----------+           |            |             |
   |                    |           |           |            |             |
 5 |                    |           |           |            |             |
   |                    |           |           |            |             |
 4 |                    |           +-----------+            |             |
   |                    |           |           |            |             |
 3 +--------+           |           |           |            |             |
   |        |           |           |           |            |             |
 2 |        |           |           |           |            |             |
   |        |           |           |           |            |             |
 1 |        +-----------+           |           |            |             |
   |        |           |           |           |            |             |
 0 +--------+-----------+-----------+-----------+------------+-------------+
         1  	     2         3           4            5           6

```

Then it begins to rain. Per hour one unit of rain falls on each segment of the landscape.
Water that falls on segment 3 in the above sample landscape will flow equally into
segment 2 and into segment 4, until segment 4 is filled up to the level of segment 3.
Water that falls on segment 5 will flow into segment 4, because segment 6 is higher.
Water that falls on segment 6 will flow into segment 5 (and then further into segment 4)
because right to segment 6 is an infinite wall (same as left to segment 1).
To the very left and to the very right of the landscape are infinite walls, i.e. water does
not flow outside of the defined landscape.
The program shall calculate the water levels for each segment after x number of hours of
rain.
The user of the program shall be able to define the landscape when running the program
and shall also be able to define the number of hours it will rain.
Describe an algorithm and its asymptotic computational complexity, including the
formal proof of the correctness (i.e. proof by induction or by any other means). The
implementation should be done in Rust or TypeScript.
Please deploy the web application to a web-server and provide us a URL, so that we can
test it. Furthermore, please attach us a link to the repository (i.e. GitHub, GitLab).


## Pre-requisite

To include the interactive client, wasm-pack needs to be installed.

```sh
 rustup target add wasm32-unknown-unknown
 cargo install wasm-pack
```

## Using locally

```sh
git clone --depth=1 https://gitlab.com/ivanceras/assignment-water-level2
cd assignment-water-level2
cd client && wasm-pack build --release --target=web && cd ..
cargo run --release
```

```sh
curl --data '{"landscape":[3,1,6,4,8,9],"rain_duration":1.0}' -X POST -H "Content-Type: application/json" http://localhost:3030/api/
```

An example using the sample landscape above is located in `examples/sample.rs`, which can be run using:

```sh
cargo run --example sample
```

Alternatively, there is an interactive client at  https://localhost:3030
which can call to the api and display the result.

## To use different port
Set the PORT environment variable to use a different port for the server.

Example:
```
PORT=8080 cargo run --releae
```

## Using the live demo
A live [demo](https://ivanceras-water-level2.herokuapp.com) is available in heroku.

```
curl --data '{"landscape":[3,1,6,4,8,9],"rain_duration":1.0}' -X POST -H "Content-Type: application/json" https://ivanceras-water-level2.herokuapp.com/api/
```
