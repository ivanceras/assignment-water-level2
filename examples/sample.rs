use water_level::water::Landscape;

fn main() {
    let heights = vec![3, 1, 6, 4, 8, 9];
    let rain_duration = 1.0;

    let mut run = Landscape::new(&heights);
    let new_heights = run.calculate(rain_duration);
    assert_eq!(vec![4.0, 4.0, 6.0, 6.0, 8.0, 9.0], new_heights);
    dbg!(new_heights);
}
