mod proof;

/// Group contains the segments which corresponds to the heights specified in the landscape.
/// Initially, each segment will be in a separate group. As additional water is added
/// the groups will change total heights and 2 adjacent groups with the same height
/// will be merged together into 1 group.
#[derive(Debug, Clone)]
struct Group {
    /// the height of each segments in this group
    segments: Vec<i32>,
    /// segment height + water level
    total_height: f32,
}

impl Group {
    /// create a group with 1 member
    fn new(height: i32) -> Self {
        Self {
            total_height: height as f32,
            segments: vec![height],
        }
    }

    /// group can be merged together when they have the same height
    /// Note: we use f32::EPSILON here as the error margin, taking into account floating point
    /// precision errors
    fn can_merge(&self, other: &Self) -> bool {
        (self.total_height - other.total_height).abs() < f32::EPSILON
    }

    /// dissolve `other` group into this group
    /// extending `other's` segments into this group
    fn merge(&mut self, other: &Self) {
        self.segments.extend(&other.segments)
    }

    /// Returns the amount of water accumulated on top of each segment of this group.
    fn water_volume(&self) -> Vec<f32> {
        self.segments
            .iter()
            .map(|height| self.total_height - *height as f32)
            .collect()
    }
    /// Returns the rounded(in 4 decimal places) amount of water accumulated on top of each segment of this group.
    fn rounded_water_volume(&self) -> Vec<f32> {
        self.water_volume().into_iter().map(round4).collect()
    }

    /// Returns the height of each segments of the landscape that are in this group.
    /// The is the sum of the segment height + the accumulated water on top of it.
    fn segment_heights(&self) -> Vec<f32> {
        self.segments
            .iter()
            .map(|_| round4(self.total_height))
            .collect()
    }
}

/// round a value to 4 decimal places
fn round4(v: f32) -> f32 {
    (v * 10_000.0).round() / 10_000.0
}

/// We do the calculation on this struct.
#[derive(Debug)]
pub struct Landscape {
    groups: Vec<Group>,
}

impl Landscape {
    /// create a landscape with the corresponding heights
    pub fn new(heights: &[i32]) -> Self {
        let groups = heights.iter().map(|h| Group::new(*h)).collect();
        Self { groups }
    }

    /// return the height of the left and right of group at index `idx`
    fn neighbor_heights(&self, idx: usize) -> (f32, f32) {
        let last_idx = self.groups.len() - 1;
        let left = if idx == 0 {
            f32::INFINITY
        } else {
            self.groups[idx - 1].total_height
        };
        let right = if idx == last_idx {
            f32::INFINITY
        } else {
            self.groups[idx + 1].total_height
        };

        (left, right)
    }

    /// return the height of the group at index `idx`
    fn height_at(&self, idx: usize) -> f32 {
        self.groups[idx].total_height
    }

    /// return the total segments in this group at index `idx`
    fn segments_in_group(&self, idx: usize) -> usize {
        self.groups[idx].segments.len()
    }

    /// get holding capacity of this group, before it starts overflow to its neighboring group
    fn capacity(&self, idx: usize) -> f32 {
        let (left, right) = self.neighbor_heights(idx);
        let height = self.height_at(idx);
        if height > left || height > right {
            0.0
        } else {
            (left.min(right) - height) * self.segments_in_group(idx) as f32
        }
    }

    /// Group together adjacent group that has the equal `total_height`
    /// and return the new group index of `grp_idx`.
    ///
    /// Algorithm: Iterate through each of the group and check if this group can
    /// be merged with last element of the newly formed new group `new_groups`.
    ///
    ///
    ///  Complexity: O(n)
    fn regroup_and_track(&mut self, grp_idx: usize) -> usize {
        let mut new_grp_idx = None;
        let mut new_groups = vec![];
        for (idx, group) in self.groups.iter().enumerate() {
            let last_merged_group: Option<&mut Group> = new_groups.last_mut();
            if let Some(last_merged_group) = last_merged_group {
                if last_merged_group.can_merge(group) {
                    last_merged_group.merge(group);
                } else {
                    new_groups.push(group.clone());
                }
            } else {
                new_groups.push(group.clone())
            }
            // the new index of `grp_idx` is the last element of new_groups
            if grp_idx == idx {
                new_grp_idx = Some(new_groups.len() - 1);
            }
        }
        self.groups = new_groups;
        if let Some(new_grp_idx) = new_grp_idx {
            new_grp_idx
        } else {
            // if no grp_idx in the iteration, it must be last element of the new group
            self.groups.len() - 1
        }
    }

    /// Add a rain to this group at index `idx` with an amount of water `amt_water`
    ///
    /// # Arguments:
    /// - `idx` - the index of the group to add the rain
    /// - `amt_water` - the amount of rain/water to be added.
    ///
    /// # Algorithm:
    /// Calculate the amount of water than can be added to the group at index `idx`
    /// before it starts to overflow.
    /// This amount is then added to the group's `total_height`
    /// The overflowed water amount will be passed to the left and/or right depending on the
    /// following circumstances:
    /// - If the overflow water amount of this group can be moved to the right and to the right.
    ///   that overflow water amount is divided equally for the left and right neighboring group,
    ///   using the same recursive call to this function.
    /// - If the overflow water amount can only be moved to the left, all of it will be added to
    /// the left using the recursive call to this function.
    /// - If the overflow water amount can only be moved to the right, all of it will be added to
    /// the right using the recursive call to this function.
    /// - If there is an overflow, but can neither be moved to the left nor to the right.
    ///    This means that this group has leveled with the neighboring group. A regroup call is
    ///    needed to merge this group together, and finally a recursive function call to this
    ///    function with overflow as the amount of water. This overflow will then be divided in the
    ///    same fashion as from start.
    ///
    ///
    /// # Complexity: O(log(n))
    ///
    fn add_water_to_group(&mut self, idx: usize, amt_water: f32, mut recursion: usize) {
        dbg!(&self);
        println!("old_idx: {}", idx);
        let idx = self.regroup_and_track(idx);
        println!("new_idx: {}", idx);
        let indent = "\t".repeat(recursion);
        println!("{}Adding to group {}", indent, idx);
        println!(
            "{}Adding to group {}: {:?} with amount of {}, in recursion: {}",
            indent, idx, self.groups[idx], amt_water, recursion
        );
        let capacity = self.capacity(idx);
        let overflow = (amt_water - capacity).max(0.0);
        let added = amt_water - overflow;
        let per_segment_added = added / self.groups[idx].segments.len() as f32;
        self.groups[idx].total_height += per_segment_added;

        let (left, right) = self.neighbor_heights(idx);
        let height = self.height_at(idx);
        let can_overflow_left = height > left;
        let can_overflow_to_right = height > right;
        if can_overflow_left && can_overflow_to_right {
            println!("{} Overflowing to left and right", indent);
            recursion += 1;
            self.add_water_to_group(idx - 1, overflow / 2.0, recursion);
            self.add_water_to_group(idx + 1, overflow / 2.0, recursion);
        } else if can_overflow_left {
            println!("{} Overflowing to left", indent);
            recursion += 1;
            self.add_water_to_group(idx - 1, overflow, recursion);
        } else if can_overflow_to_right {
            println!("{} Overflowing to right", indent);
            recursion += 1;
            self.add_water_to_group(idx + 1, overflow, recursion);
        }
        // Capacity of either left or right is filled but there is still excess overflow, regroup(due to height changes) and re-add overflow
        if overflow > 0.0 && !can_overflow_left && !can_overflow_to_right {
            println!(
                "{}--->Capacity of either left or right is filled but there is still excess overflow, regroup(due to height changes) and re-add overflow of: {}",
                indent, overflow);

            recursion += 1;
            self.add_water_to_group(idx, overflow, recursion);
        }
    }

    /// Determine which group this segment with index `idx` belongs to.
    ///
    fn segment_in_group(&self, segment_idx: usize) -> usize {
        println!("finding group for segment_idx: {}", segment_idx);
        let mut segment_cnt = 0;
        for (grp_idx, group) in self.groups.iter().enumerate() {
            if segment_cnt >= segment_idx {
                return grp_idx;
            }
            let segments = group.segments.len();
            segment_cnt += segments;
        }
        self.groups.len() - 1
    }

    /// Add a rain water amount of `amt_water` at segment at index `idx`.
    /// # Argument
    /// - `segment_idx` the index of the segment in the same arrangement as the landscape heights
    /// parameter.
    /// - `amt_water` the amount of water to be added to this segment of the landscape
    ///
    /// # Complexity: O(log(n))
    fn add_rain_to_segment(&mut self, segment_idx: usize, amt_water: f32) {
        println!("Rain to segment: {}", segment_idx);
        let group_idx = self.segment_in_group(segment_idx);
        self.add_water_to_group(group_idx, amt_water, 0);
    }

    /// Returns the water volume of each segment in this landscape.
    #[allow(unused)]
    fn water_volume(&self) -> Vec<f32> {
        self.groups.iter().flat_map(|g| g.water_volume()).collect()
    }

    /// Returns the rounded(in 4 decimal places) water volume of each segment in this landscape.
    #[allow(unused)]
    fn rounded_water_volume(&self) -> Vec<f32> {
        self.groups
            .iter()
            .flat_map(|g| g.rounded_water_volume())
            .collect()
    }

    /// return the heights of each segment on the landscape.
    /// This is the sum height of the landscape + the water accumulated on that segment.
    pub fn segment_heights(&self) -> Vec<f32> {
        self.groups
            .iter()
            .flat_map(|g| g.segment_heights())
            .collect()
    }

    /// With the specified rain duration `rain_duration`,
    /// run the algorithm which calculates the amount of water to be accumulated on each
    /// segment in this landscape, and return the final heights of each segment.
    ///
    /// # Argument
    /// - `rain_duration` - the duration of the rain.
    ///
    /// # Complexity: O(n(log(n))
    pub fn calculate(&mut self, rain_duration: f32) -> Vec<f32> {
        for i in 0..self.groups.len() {
            dbg!(&self);
            self.add_rain_to_segment(i, 1.0 * rain_duration);
        }
        dbg!(&self);
        self.segment_heights()
    }

    /// Get the total rain volume of this landscape by adding each of the water_level
    /// in each of the segments. The value is rounded to 4 decimal places.
    #[cfg(test)]
    fn total_water_volume(&self) -> f32 {
        let sum: f32 = self.rounded_water_volume().iter().sum();
        round4(sum)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn segment_to_group_should_be_correct() {
        let heights = vec![3, 3, 6, 4, 8, 9];
        let mut landscape = Landscape::new(&heights);
        dbg!(&landscape);
        //before regroup_and_track
        assert_eq!(2, landscape.segment_in_group(2));
        assert_eq!(5, landscape.segment_in_group(5));
        landscape.regroup_and_track(0);
        dbg!(&landscape);
        // after regroup_and_track
        assert_eq!(1, landscape.segment_in_group(2));
    }

    #[test]
    fn regroup_tracking_old_grp_should_work() {
        let heights = vec![3, 3, 3, 4, 8, 9];
        let mut landscape = Landscape::new(&heights);
        dbg!(&landscape);
        assert_eq!(0, landscape.regroup_and_track(2));
    }

    #[test]
    fn basic_test() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 1.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![4.0, 4.0, 6.0, 6.0, 8.0, 9.0], new_heights);
    }

    #[test]
    fn rain_duration2() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 2.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());

        assert_eq!(vec![6.5, 6.5, 6.5, 6.5, 8.0, 9.0], new_heights);
    }

    #[test]
    fn rain_duration1_5() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 1.5;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![5.5, 5.5, 6.0, 6.0, 8.0, 9.0], new_heights);
    }

    #[test]
    fn rain_duration3() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 3.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![8.0, 8.0, 8.0, 8.0, 8.0, 9.0], new_heights);
    }

    #[test]
    fn more_entries1() {
        let heights = vec![3, 1, 6, 4, 8, 9, 1];
        let rain_duration = 1.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![3.75, 3.75, 6.0, 6.0, 8.0, 9.0, 2.5], new_heights);
    }

    #[test]
    fn more_entries5() {
        let heights = vec![3, 1, 6, 4, 8, 9, 1];
        let rain_duration = 5.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(
            vec![9.5714, 9.5714, 9.5714, 9.5714, 9.5714, 9.5714, 9.5714],
            new_heights
        );
        assert!((calc_rain_volume - landscape.total_water_volume()).abs() < 0.001);
    }

    #[test]
    fn more_entries2() {
        let heights = vec![1, 3, 1, 6, 4, 8, 9];
        let rain_duration = 1.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(
            vec![3.3333, 3.3333, 3.3333, 6.0, 6.0, 8.0, 9.0],
            new_heights
        );
        assert!((calc_rain_volume - landscape.total_water_volume()).abs() < 0.001);
    }

    #[test]
    fn high_wall_at_the_right() {
        let heights = vec![0, 0, 0, 0, 0, 10];
        let rain_duration = 1.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![1.2, 1.2, 1.2, 1.2, 1.2, 10.0], new_heights);
    }

    #[test]
    fn high_wall_at_the_left() {
        let heights = vec![9, 2, 2, 2, 2, 2];
        let rain_duration = 1.0;
        let calc_rain_volume = heights.len() as f32 * rain_duration;

        let mut landscape = Landscape::new(&heights);
        let new_heights = landscape.calculate(rain_duration);

        assert_eq!(calc_rain_volume, landscape.total_water_volume());
        assert_eq!(vec![9.0, 3.2, 3.2, 3.2, 3.2, 3.2], new_heights);
    }

    #[test]
    fn flat_landscape_should_have_uniform_water_level_at_certain_duration() {
        let segments_len = 6;
        let heights = vec![0; segments_len];
        for i in 0..100 {
            let rain_duration = i as f32;
            let calc_rain_volume = heights.len() as f32 * rain_duration;
            let mut landscape = Landscape::new(&heights);
            let new_heights = landscape.calculate(rain_duration);
            assert_eq!(calc_rain_volume, landscape.total_water_volume());
            let water_level = i as f32;
            assert_eq!(vec![water_level; segments_len], new_heights);
        }
    }

    #[test]
    fn flat_landscape_should_have_uniform_water_at_ever_increasing_landscape_segments() {
        for i in 0..10 {
            let segments_len = i;
            let heights = vec![0; segments_len];
            for j in 0..100 {
                let rain_duration = j as f32;
                let calc_rain_volume = heights.len() as f32 * rain_duration;
                let mut landscape = Landscape::new(&heights);
                let new_heights = landscape.calculate(rain_duration);
                assert_eq!(calc_rain_volume, landscape.total_water_volume());
                let water_level = j as f32;
                assert_eq!(vec![water_level; segments_len], new_heights);
                assert_eq!(vec![water_level; segments_len], landscape.segment_heights());
            }
        }
    }
}
