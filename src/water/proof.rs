//! Proof
//!
//! Proof of algorithm in water module is correct
//! by using running a simulation based on the observation on how water flows in the real world.
//!
#[allow(warnings)]
use super::*;

#[derive(Debug, PartialEq)]
enum Finished {
    AtIteration(usize),
    ReachedMaxIteration(usize),
}

trait Proof {
    fn run(
        &mut self,
        rain_duration: f32,
        stop_flow: Option<f32>,
        max_iteration: Option<usize>,
    ) -> (Finished, Vec<f32>);

    fn simulate_flow(&mut self, stop_flow: f32) -> bool;
}

impl Proof for Landscape {
    /// Run a water flow simulation by continously moving half of the difference of
    /// water level to adjacent neighbor
    ///
    /// # Complexity:  O(n) * rain_duration
    fn run(
        &mut self,
        rain_duration: f32,
        stop_flow: Option<f32>,
        max_iteration: Option<usize>,
    ) -> (Finished, Vec<f32>) {
        for group in self.groups.iter_mut() {
            group.total_height += 1.0 * rain_duration;
        }

        let stop_flow = stop_flow.unwrap_or(0.0);

        if let Some(max_iteration) = max_iteration {
            for i in 0..max_iteration {
                if !self.simulate_flow(stop_flow) {
                    return (Finished::AtIteration(i), self.segment_heights());
                }
            }
            (
                Finished::ReachedMaxIteration(max_iteration),
                self.segment_heights(),
            )
        } else {
            let mut iteration = 0;
            loop {
                if !self.simulate_flow(stop_flow) {
                    return (Finished::AtIteration(iteration), self.segment_heights());
                }
                iteration += 1;
            }
        }
    }

    /// Simulate flow of water to neighbors affecting their heights
    /// returns true if it is still flowing
    ///
    /// we can only move an amount of water that is not greater than the water_level
    /// of this segment.
    ///
    /// if the amount of water to be overflow is greater than the capacity of it's
    /// neigbor, we move half the volume of the sum of overflow amount and the capacity amount.
    ///
    ///    3 .~~~~~~~.        |
    ///      !  A(1) !        |
    ///    2 +-------+ B(0.5) |
    ///      |       |________|
    ///    1 |       |        |
    ///
    /// In the figure, Segment A height is 2 and Segment B height is 1.5
    /// Segment A has an accumulated rain of 1 unit volume while Segment B
    /// has a capacity of 0.5 unit volume before it can overflow.
    /// The amount of water that needs to be transferred from Segment A to B
    /// should only be 0.75 to put it in an equilibrium state.
    /// The final height of Segment A will be 2.25 and Segment B is also 2.25
    ///
    /// The formula:  (water_volume of A + capacity of B) / 2.0
    ///
    /// # Approximate Complexity:
    ///    - O(n*I), where I = iterations roughly equal to (rain_duration * groups * 10);
    ///    - let say I = n^2, then complexity is about O(n^3)
    fn simulate_flow(&mut self, stop_flow: f32) -> bool {
        let groups_len = self.groups.len();
        let mut overflows: Vec<(f32, f32)> = Vec::with_capacity(groups_len);
        for (i, group) in self.groups.iter().enumerate() {
            let total_height = group.total_height;
            let heights = &group.segments;
            // only 1 segment per group since we didnt regroup the segments
            assert_eq!(heights.len(), 1);
            let base_height = heights[0];

            let volumes: Vec<f32> = group.water_volume();
            // only 1 volume since we didnt regroup the segments
            assert_eq!(volumes.len(), 1);
            let volume = volumes[0];
            let (left_height, right_height) = self.neighbor_heights(i);

            let can_overflow_to_left = total_height > left_height;
            let can_overflow_to_right = total_height > right_height;

            let left_diff = if let Some(left) = self.groups.get((i as i32 - 1) as usize) {
                base_height as f32 - left.total_height
            } else {
                f32::INFINITY
            };

            // the maximum amount of water from this landscape that can be move to the right
            let right_diff = if let Some(right) = self.groups.get(i + 1) {
                base_height as f32 - right.total_height
            } else {
                f32::INFINITY
            };

            println!(
                "height: {} diff: ({},{})",
                base_height, left_diff, right_diff
            );

            let (left_out, right_out) = if can_overflow_to_left && can_overflow_to_right {
                let half_vol = volume / 2.0;
                let left_vol = half_vol.min((left_diff + half_vol) / 2.0);
                let right_vol = half_vol.min((right_diff + half_vol) / 2.0);
                (left_vol, right_vol)
            } else if can_overflow_to_left && !can_overflow_to_right {
                (volume.min((left_diff + volume) / 2.0), 0.0)
            } else if !can_overflow_to_left && can_overflow_to_right {
                (0.0, volume.min((right_diff + volume) / 2.0))
            } else {
                (0.0, 0.0)
            };

            overflows.push((left_out, right_out));
        }

        dbg!(&self);
        dbg!(&overflows);
        // track how many segments are still flowing
        let mut flowing_segments = 0;
        for (i, (left_out, right_out)) in overflows.iter().enumerate() {
            let total_out = left_out + right_out;
            if total_out > stop_flow {
                flowing_segments += 1;
            }
            self.groups[i].total_height -= total_out;
            if let Some(left) = self.groups.get_mut((i as i32 - 1) as usize) {
                left.total_height += left_out;
            }
            if let Some(right) = self.groups.get_mut(i + 1) {
                right.total_height += right_out;
            }
        }
        flowing_segments > 0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_proof1() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 1.0;

        let mut landscape = Landscape::new(&heights);
        let (finish, new_heights) = landscape.run(rain_duration, None, None);

        dbg!(&finish);
        assert_eq!(vec![4.0, 4.0, 6.0, 6.0, 8.0, 9.0], new_heights);
        assert_eq!(Finished::AtIteration(46), finish);
    }

    #[test]
    fn basic_proof2() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 2.0;

        let mut landscape = Landscape::new(&heights);
        let (finish, new_heights) = landscape.run(rain_duration, None, None);

        dbg!(&finish);
        assert_eq!(vec![6.5, 6.5, 6.5, 6.5, 8.0, 9.0], new_heights);
        assert_eq!(Finished::AtIteration(50), finish);
    }

    #[test]
    fn basic_proof3() {
        let heights = vec![3, 1, 6, 4, 8, 9];
        let rain_duration = 3.0;

        let mut landscape = Landscape::new(&heights);
        let (finish, new_heights) = landscape.run(rain_duration, Some(0.000001), None);

        dbg!(&finish);
        assert_eq!(vec![8.0, 8.0, 8.0, 8.0, 8.0, 9.0], new_heights);
        assert_eq!(Finished::AtIteration(72), finish);
    }

    // This is close to my solution
    // but due to how this is calculated here,
    // the water from segment 9, seems to have bounced 1/8
    // of that water to the right, due to the fact that we drop
    // all the rain water in each segment all together, instead of 1 by 1.
    //
    // Since each iteration progress at a step, the water has not fully settled
    // to lower areas hence the bouncing effect.
    #[test]
    fn oddcase() {
        let heights = vec![3, 1, 6, 4, 8, 9, 1];
        let rain_duration = 1.0;

        let mut landscape = Landscape::new(&heights);
        let (finish, new_heights) = landscape.run(rain_duration, Some(0.000001), None);

        dbg!(&finish);
        assert_eq!(vec![3.6875, 3.6875, 6.0, 6.0, 8.0, 9.0, 2.625], new_heights);
        assert_eq!(Finished::AtIteration(39), finish);
    }
}
