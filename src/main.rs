#![deny(warnings)]

use client::{App, Data, FetchStatus, Params};
use sauron::Render;
use std::net::SocketAddr;
use warp::http::{self, Response};
use warp::Filter;
use water::Landscape;

#[macro_use]
extern crate log;

mod page;
mod water;

const PKG_DIR: &str = "client/pkg";
const FAVICON_FILE: &str = "client/favicon.ico";
const DEFAULT_PORT: u16 = 3030;

fn compute_water_levels(arg: &Params) -> Vec<f32> {
    let mut landscape = Landscape::new(&arg.landscape);
    landscape.calculate(arg.rain_duration)
}

fn render_page(app: &App) -> Result<Response<String>, http::Error> {
    Response::builder().body(page::index(app).render_to_string())
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let json_post = warp::path("api")
        .and(warp::body::json::<Params>())
        .map(|params: Params| {
            let water_level = compute_water_levels(&params);
            debug!("water level: {:?}", water_level);
            serde_json::to_string(&water_level).unwrap()
        });

    let root = warp::path::end().map(move || {
        let mut app = App::default();
        app.data = FetchStatus::Complete(Data {
            water_levels: compute_water_levels(&app.params),
        });
        render_page(&app)
    });
    let pkg_files = warp::path("pkg")
        .and(warp::fs::dir(PKG_DIR))
        .with(warp::compression::gzip());

    let favicon = warp::path("favicon.ico").and(warp::fs::file(FAVICON_FILE));
    let form_submit =
        warp::path("submit")
            .and(warp::query())
            .map(move |params: Vec<(String, String)>| {
                let mut heights = vec![];
                let mut rain_duration = 1.0;
                for (key, value) in params {
                    match &*key {
                        "landscape" => {
                            if let Ok(value) = value.parse::<i32>() {
                                heights.push(value);
                            }
                        }
                        "rain_duration" => {
                            if let Ok(value) = value.parse::<f32>() {
                                rain_duration = value;
                            }
                        }
                        _ => warn!("ignoring {:?}", key),
                    }
                }
                let params = Params {
                    landscape: heights,
                    rain_duration,
                };
                let data = Data {
                    water_levels: compute_water_levels(&params),
                };
                let app = App::with_params_and_data(params, data);
                render_page(&app)
            });

    let routes = warp::get()
        .and(root.or(pkg_files).or(favicon).or(form_submit))
        .or(json_post);

    let port = if let Ok(port) = std::env::var("PORT") {
        if let Ok(port) = port.parse::<u16>() {
            port
        } else {
            DEFAULT_PORT
        }
    } else {
        DEFAULT_PORT
    };

    let socket: SocketAddr = ([0, 0, 0, 0], port).into();
    println!("serve at http://{}:{}", socket.ip(), socket.port());
    warp::serve(routes).run(socket).await;
}
