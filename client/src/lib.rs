#![deny(warnings)]

use sauron::js_sys::TypeError;
use sauron::prelude::*;
use sauron::web_sys::Headers;
use sauron::web_sys::RequestInit;
use sauron::web_sys::Response;
use serde::{Deserialize, Serialize};

#[macro_use]
extern crate log;

const DATA_URL: &'static str = "/api";
const DEFAULT_RAIN_DURATION: f32 = 1.0;
const DEFAULT_LANDSCAPE: [i32; 6] = [3, 1, 6, 4, 8, 9];

#[derive(Debug, Serialize, Deserialize)]
pub enum FetchStatus<T> {
    Idle,
    Loading,
    Complete(T),
    Error(Option<String>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Msg {
    EditRainDuration(String),
    EditLandscape(usize, String),
    AddMoreLandscape,
    EditParams(String),
    ReceivedData(Result<Data, Response>),
    RequestError(TypeError),
    QueryAPI,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct Data {
    pub water_levels: Vec<f32>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Params {
    pub landscape: Vec<i32>,
    pub rain_duration: f32,
}

impl Default for Params {
    fn default() -> Self {
        Params {
            landscape: DEFAULT_LANDSCAPE.to_vec(),
            rain_duration: DEFAULT_RAIN_DURATION,
        }
    }
}

// App and all its members should be Serializable by serde
#[derive(Debug, Deserialize, Serialize)]
pub struct App {
    pub params: Params,
    pub data: FetchStatus<Data>,
    error: Option<String>,
}

impl Default for App {
    fn default() -> Self {
        App {
            params: Params::default(),
            data: FetchStatus::Idle,
            error: None,
        }
    }
}

impl App {
    pub fn with_params_and_data(params: Params, data: Data) -> Self {
        Self {
            params,
            data: FetchStatus::Complete(data),
            error: None,
        }
    }

    fn fetch_data(&self) -> Cmd<Self, Msg> {
        let url = format!("{}/", DATA_URL);
        let json_txt = serde_json::to_string_pretty(&self.params).expect("must serialize");

        debug!("json_txt: {:?}", json_txt);
        let mut json_body = RequestInit::new();
        json_body.method("POST");
        let headers = Headers::new().expect("must construct headers");
        headers
            .append("Content-Type", "application/json")
            .expect("should append header");

        json_body.headers(&headers);
        json_body.body(Some(&JsValue::from_str(&json_txt)));

        Http::fetch_with_request_and_response_decoder(
            &url,
            Some(json_body),
            |(code, txt, _headers)| {
                trace!("got code: {} txt: {}", code, txt);
                if code == 200 {
                    if let Ok(water_levels) = serde_json::from_str::<Vec<f32>>(&txt) {
                        let data = Data { water_levels };
                        Msg::ReceivedData(Ok(data))
                    } else {
                        let err_resp = Response::new().expect("must construct");
                        Msg::ReceivedData(Err(err_resp))
                    }
                } else {
                    let err_resp = Response::new().expect("must construct");
                    Msg::ReceivedData(Err(err_resp))
                }
            },
            Msg::RequestError,
        )
    }
}

impl Component<Msg> for App {
    fn init(&self) -> Cmd<Self, Msg> {
        Cmd::none()
    }

    fn view(&self) -> Node<Msg> {
        let params_json = serde_json::to_string_pretty(&self.params).expect("must serialize");
        debug!("params: {:?}", params_json);

        node! {
            <main>
                <form on_submit={|e| {
                    e.prevent_default();
                    Msg::QueryAPI
                }} method="GET" action="/submit" >
                    <div class="landscape">
                        <label>
                            "Landscape heights:"
                            {for (i,ls) in self.params.landscape.iter().enumerate(){
                                 node!{
                                    <input
                                        name="landscape"
                                        type="number"
                                        value={ls.to_string()}
                                        on_input={move|e| Msg::EditLandscape(i, e.value)}
                                    />
                                 }
                            }}
                            <button on_click={|_e|Msg::AddMoreLandscape}>"+"</button>
                        </label>
                    </div>
                    <label class="rain_duration">
                        "Rain duration"
                        <input
                            name="rain_duration"
                            type="number"
                            step="any"
                            value={self.params.rain_duration}
                            on_input={|e| Msg::EditRainDuration(e.value)}
                        />
                    </label>
                    <label>
                        "Parameters"
                        <textarea
                            name="params"
                            type="number"
                            rows="12"
                            cols="40"
                            value={params_json}
                            on_input={|e| Msg::EditParams(e.value)}
                        >{text(&params_json)}</textarea>
                    </label>
                    <button type="submit">"Calculate"</button>
                </form>
                {self.view_data()}
            </main>
        }
    }

    fn update(&mut self, msg: Msg) -> Cmd<Self, Msg> {
        trace!("App is updating from msg: {:?}", msg);
        let mut cmd = Cmd::none();
        match msg {
            Msg::EditRainDuration(rain_duration) => {
                debug!("rain duration: {}", rain_duration);
                self.params.rain_duration = if let Ok(rain_duration) = rain_duration.parse::<f32>()
                {
                    rain_duration
                } else {
                    DEFAULT_RAIN_DURATION
                };
            }
            Msg::EditParams(params) => {
                debug!("params: {:?}", params);
                if let Ok(params) = serde_json::from_str::<Params>(&params) {
                    self.params = params;
                }
            }
            Msg::EditLandscape(idx, height) => {
                if let Ok(height) = height.parse::<i32>() {
                    if let Some(ls) = self.params.landscape.get_mut(idx) {
                        *ls = height
                    }
                }
            }
            Msg::AddMoreLandscape => {
                self.params.landscape.push(0);
            }
            Msg::QueryAPI => {
                self.data = FetchStatus::Loading;
                cmd = self.fetch_data()
            }
            Msg::ReceivedData(Ok(data)) => self.data = FetchStatus::Complete(data),
            Msg::ReceivedData(Err(js_value)) => {
                trace!("Error fetching data! {:#?}", js_value);
                self.data = FetchStatus::Error(Some(format!(
                    "There was an error reaching the api: {:?}",
                    js_value
                )));
            }
            Msg::RequestError(type_error) => {
                trace!("Error requesting the page: {:?}", type_error);
                self.data = FetchStatus::Error(Some(format!(
                    "There was an error fetching the page: {:?}",
                    type_error
                )));
            }
        };
        cmd
    }
}

impl App {
    fn view_data(&self) -> Node<Msg> {
        match &self.data {
            FetchStatus::Idle => node! { <p>"Waiting around..."</p> },
            FetchStatus::Error(Some(e)) => {
                node! {
                    <article>
                        <p>"Okay, something went wrong. I think it was: "</p>
                        <code>{text(e)}</code>
                    </article>
                }
            }
            FetchStatus::Error(None) => {
                node! {
                    <article>
                        <p>"Okay, something went wrong. I have no idea what it is."</p>
                    </article>
                }
            }
            FetchStatus::Loading => {
                node! {
                    <article>
                        <p>"Loading..."</p>
                    </article>
                }
            }
            FetchStatus::Complete(data) => {
                node! {
                    <article class="result">
                        <p>"Water levels: "
                        {
                            for level in data.water_levels.iter(){
                                node!{
                                    <span class="water-level">{text!("{}, ",level)}</span>
                                }
                            }
                        }
                        </p>
                        <p>
                            {self.display_graph(&data)}
                        </p>
                    </article>
                }
            }
        }
    }

    fn display_graph(&self, data: &Data) -> Node<Msg> {
        let landscape = &self.params.landscape;
        let water_level = &data.water_levels;
        let water_volume: Vec<f32> = landscape
            .iter()
            .zip(water_level.iter())
            .map(|(ls, level)| *level - *ls as f32)
            .collect();
        info!("water volume: {:?}", water_volume);
        let graph_width = 800.0;
        let graph_height = 200.0;

        let max_level = water_level
            .iter()
            .max_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal))
            .map(|l| *l)
            .unwrap_or(0.0);

        let segments = landscape.len();

        let unit_x = graph_width / segments as f32;

        let unit_y = graph_height / max_level;

        // (height, volume)
        let dimensions: Vec<(f32, f32)> = landscape
            .iter()
            .map(|h| *h as f32)
            .zip(water_volume.iter().map(|v| *v))
            .collect();

        node! {
            <svg width={graph_width} height={graph_height} xmlns="http://www.w3.org/2000/svg">
              <defs>
                <pattern id="units" x="0" y="0" width={ unit_x } height={ unit_y } patternUnits="userSpaceOnUse">
                  <rect x="0" y="0" width={ unit_x - 1.0 } height={ unit_y - 1.0 } fill="rgb(200, 200, 200)"/>
                </pattern>
              </defs>
              <rect id="grid_background" fill="url(#units)" stroke="black" width={graph_width} height={graph_height}/>
                {
                    for (i,(landscape_height, volume)) in dimensions.iter().enumerate(){
                        let scaled_landscape_height = landscape_height  * unit_y;
                        let scaled_volume_height  = volume * unit_y;
                        node!{
                            <g>
                                <rect class="earth" x={ i as f32 * unit_x } y={ graph_height - scaled_landscape_height } width={ unit_x - 1.0 } height={ scaled_landscape_height } fill="green"/>
                                <rect class="water" x={ i as f32 * unit_x } y={ graph_height - scaled_landscape_height - scaled_volume_height } width={ unit_x - 1.0 } height={ scaled_volume_height } fill="blue"/>
                            </g>
                        }
                    }
                }
            </svg>
        }
    }
}

/// The serialized_state is supplied by the generated page from the webserver.
/// The generated page in index function has a main function which is supplied by a json text
/// serialized state. This json text is deserialized and used here as our `App` value which
/// will then be injected into the view
#[wasm_bindgen]
pub fn main(serialized_state: String) {
    console_log::init_with_level(log::Level::Trace).unwrap();
    trace!("start here..");
    console_error_panic_hook::set_once();

    let app = if let Ok(app) = serde_json::from_str::<App>(&serialized_state) {
        app
    } else {
        App::default()
    };

    match web_sys::window() {
        Some(window) => {
            trace!("found window, will try to replace <main>");
            let document = window.document().expect("should have a document on window");
            Program::new_replace_mount(
                app,
                &document
                    .query_selector_all("main")
                    .expect("must have the main element in the document")
                    .get(0)
                    .expect("Must have the first element"),
            );
        }
        None => {
            trace!("window not found");
            Program::mount_to_body(app);
        }
    }
}
