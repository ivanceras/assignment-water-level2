cd client && wasm-pack build --release --target=web && cd ..

# we check in the pkg directory from our local wasm-pack build to heroku
# since the setup was only designed for rust build-pack
rm client/pkg/.gitignore
